public class Variable<T>{
	private String name;
	private T value;

	public Variable(String name){
		this.name = name;
	}

	public Variable(String name, T value){
		this.name=name;
		assign(value);
	}

	public void assign(T value){ 
		this.value = value;
	}

	public T getValue(){
		return value;
	}
}
