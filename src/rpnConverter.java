import java.util.HashMap;
import java.util.Stack;
import java.util.LinkedList;

public class rpnConverter{
	private HashMap<String,Integer> precedences;

	public rpnConverter(){
		precedences = new HashMap<>();
		precedences.put("=",2);
		precedences.put("+=",2);
		precedences.put("-=",2);
		precedences.put("*=",2);
		precedences.put("/=",2);
		precedences.put("_?_:_",4); //ternary
		precedences.put("||",8);
		precedences.put("&&",16);
		precedences.put("!=",32);
		precedences.put("==",32);
		precedences.put(">",64);
		precedences.put("<",64);
		precedences.put(">=",64);
		precedences.put("<=",64);
		precedences.put("instanceof",64);
		precedences.put("+",128);
		precedences.put("-",128);
		precedences.put("*",256);
		precedences.put("/",256);
		precedences.put("%",256);
		precedences.put("++_",512);
		precedences.put("--_",512);
		precedences.put("+_",512);
		precedences.put("-_",512);
		precedences.put("!",512);
		precedences.put("_++",1024);
		precedences.put("_--",1024);
		precedences.put("(",1);
		precedences.put(")",1);

	}


	public LinkedList<String> convert(String[] tokens){
		Stack<String> opStack = new Stack<>();
		LinkedList<String> result = new LinkedList<>();
		for(String tok  : tokens){
			if(tok.equals("(")){
				opStack.push(tok);
			}
			else if(tok.equals(")")){
				while(!opStack.peek().equals("(")){
					result.add(opStack.pop());
				}
				opStack.pop();
			}
			else if(!precedences.containsKey(tok)){
				result.add(tok);
			}
			else{ //token is operator
				if(opStack.empty()){
					opStack.push(tok);
				}
				else if(precedences.get(tok) > precedences.get(opStack.peek())){
					opStack.push(tok);
				}
				else if(precedences.get(tok) <= precedences.get(opStack.peek())){
					while(!opStack.empty() && precedences.get(opStack.peek()) >= precedences.get(tok)){
						result.add(opStack.pop());
					}	
					opStack.push(tok);
				}
			}
			//System.out.println("opStack -> "+opStack.toString());
			//System.out.println("result ->"+result.toString());
		}

		while(!opStack.empty()){
			result.add(opStack.pop());
		}

		return result;
	}
	
	public LinkedList<String> Lex(String str){
		StringBuilder currToken = new StringBuilder();
		LinkedList result = new LinkedList();
		for(int i = 0 ; i < str.length; i++){
			char currChar = str[i];
			if(currToken.length() == 0){
				currToken.append(currChar);
			}
			else if() //continue!
		}	

	}

	public static void main(String[] args){
		args = "( 1 + 2 + 3 + 4 ) / ( 5 * -_ 2 ) >= 7".split(" ");
		rpnConverter conv = new rpnConverter();
		System.out.println("Original: ");
		for(String arg : args){
			System.out.print(arg + " ");
		}
		System.out.println();
		LinkedList<String> result = conv.convert(args);
		System.out.println("Converted: ");
		for(String tok : result){
			System.out.print(tok+" ");
		}
		System.out.println();
	}
}
