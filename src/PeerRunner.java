import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.rmi.Naming;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import static java.lang.Integer.parseInt;

public class PeerRunner{
	public static String USAGE = "java PeerRunner [name] [ip] [port]";
	public static void main(String args[]) {
		try {

			PeerImpl thisPeer = new PeerImpl(String.format("rmi://%s:%s/%s", args[1], args[2], args[0]));
			Peer peerStub = (Peer) UnicastRemoteObject.exportObject(thisPeer, parseInt(args[2]));

			Registry registry = LocateRegistry.createRegistry(parseInt(args[2]));
			registry.bind(args[0], peerStub);

			System.out.println("PEER READY: " + args[0]);

			InputStream in = System.in;
			interact(thisPeer, in);

		} catch (Exception e) {
			System.err.println("Something went wrong");
			System.err.println("Usage: " + USAGE);
			e.printStackTrace();
			System.exit(1);
		}
	}
	
	public static void interact(PeerImpl peer, InputStream in){
		
	}
}
